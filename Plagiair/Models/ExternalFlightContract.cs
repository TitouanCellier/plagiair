﻿using Newtonsoft.Json;

namespace Plagiair.Models
{
    public class ExternalFlightContract
    {
        public string Code { get; set; }
        public string Departure { get; set; }
        public string Arrival { get; set; }


        [JsonProperty( PropertyName = "base_price" )]
        public int BasePrice { get; set; }
        public Plane Plane { get; set; }
    }
}
﻿using Newtonsoft.Json;

namespace Plagiair.Models
{
    public class Plane
    {
        public string Name { get; set; }

        [JsonProperty(PropertyName = "total_seats")]
        public int TotalSeats { get; set; }
    }
}
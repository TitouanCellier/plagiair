﻿using System;
using System.Collections.Generic;

namespace Plagiair.Models
{
    public class ReservationContract
    {

        public string Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<Option> OptionAller { get; set; }
        public IEnumerable<Option> OptionRetour { get; set; }
        public IEnumerable<Flight> Flights { get; set; }
        public DateTime? DateIn{ get; set; }
        public DateTime? DateOut { get; set; }
    }
}

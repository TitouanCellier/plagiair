﻿namespace Plagiair.Models
{
    public class Currency
    {
        public string CurrencyCode { get; set; }
        public string Rate { get; set; }
    }
}

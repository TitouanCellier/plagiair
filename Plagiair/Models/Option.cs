﻿namespace Plagiair.Models
{
    public class Option
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Price { get; set; }
    }
}

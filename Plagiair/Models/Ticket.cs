﻿using System;
using System.Collections.Generic;

namespace Plagiair.Models
{
    public class Ticket
    {
        public int Id { get; set; } 
        public string ReservationId { get; set; }  
        public Flight Flight { get; set; }

        public IEnumerable<Option> Option { get; set; }  
        public DateTime? Date { get; set; }
    }
}

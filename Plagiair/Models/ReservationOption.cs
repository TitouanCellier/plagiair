﻿namespace Plagiair.Models
{
    public class ReservationOption
    {
        public int ReservationId { get; set; }
        public int OptionId { get; set; }
    }
}

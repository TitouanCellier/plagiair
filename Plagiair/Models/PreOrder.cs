﻿using System.Collections.Generic;

namespace Plagiair.Models
{
    public class PreOrder
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<Ticket> Tickets { get; set; }
        public double TotalPrice { get; set; } 

    }
}

﻿using System.Collections.Generic;

namespace Plagiair.Models
{
    public class FlightContract
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<string> Options { get; set; }
        public IEnumerable<object> Flights { get; set; }
    }
}

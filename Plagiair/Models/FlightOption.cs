﻿namespace Plagiair.Models
{
    public class FlightOption
    {
        public int OptionId { get; set; }
        public int FlightId { get; set; }
    }
}

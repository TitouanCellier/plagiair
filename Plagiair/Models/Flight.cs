﻿
using System.Collections.Generic;
using System.Linq;

namespace Plagiair.Models
{
    public class Flight
    {
        public Flight()
        {
            AvailableOptions = Enumerable.Empty<Option>();
            StopOvers = Enumerable.Empty<string>();
        }

        public int Id { get; set; }
        public string Departure { get; set; }
        public string Arrival { get; set; }
        public int Price { get; set; }
        public int MaxPeople { get; set; }
        public IEnumerable<Option> AvailableOptions { get; set; }
        public int FirstClass { get; set; }    
        public IEnumerable<string> StopOvers { get; set; }
    }
}

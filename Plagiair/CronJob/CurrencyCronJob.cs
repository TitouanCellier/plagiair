﻿using Microsoft.Extensions.DependencyInjection;
using Plagiair.Models.CronJob;
using Plagiair.Services.Models;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Plagiair.CronJob
{
    public class CurrencyCronJob : CronJobService
    {
        private readonly IServiceScopeFactory _serviceScopeFactory;

        public CurrencyCronJob(
            IScheduleConfig<CurrencyCronJob> config, 
            IServiceScopeFactory serviceScopeFactory) 
            : base(config.CronExpression, config.TimeZoneInfo)
        {
            _serviceScopeFactory = serviceScopeFactory;
        }

        public override Task StartAsync(CancellationToken cancellationToken)
        {
            return base.StartAsync(cancellationToken);
        }

        public override Task DoWork(CancellationToken cancellationToken)
        {
            using (var scope = _serviceScopeFactory.CreateScope())
            {
                var currencyService = scope.ServiceProvider.GetService<ICurrencyService>();
                return currencyService.getAllCurrencies();
            }
        }

        public override Task StopAsync(CancellationToken cancellationToken)
        {
            return base.StopAsync(cancellationToken);
        }
    }
}

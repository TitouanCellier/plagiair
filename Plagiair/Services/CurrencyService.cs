﻿using Plagiair.Models;
using Plagiair.Services.Models;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace Plagiair.Services
{
    public class CurrencyService : ICurrencyService
    {
        readonly IHttpClientFactory _httpClientFactory;
        //public IEnumerable<Currency> Currencies;

        public CurrencyService(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
            //Currencies = Enumerable.Empty<Currency>();
        }

        public async Task<IEnumerable<Currency>> getAllCurrencies()
        {
            var currencies = new List<Currency>();
            using (var client = _httpClientFactory.CreateClient())
            {
                var response = await client.GetAsync("https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml");
                var xmlString = await response.Content.ReadAsStringAsync();
                var doc = new XmlDocument();
                doc.LoadXml(xmlString);
                var cubes = doc.GetElementsByTagName("Cube");

                foreach(XmlElement cube in cubes)
                {
                    if(cube.HasAttribute("currency"))
                    {
                        var currency = new Currency()
                        {
                            CurrencyCode = cube.GetAttribute("currency"),
                            Rate = cube.GetAttribute("rate")
                        };
                        currencies.Add(currency);
                    }
                }

                return currencies;
            }
        }
    }
}

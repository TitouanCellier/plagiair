﻿using Newtonsoft.Json;
using Plagiair.Models;
using Plagiair.Services.Models;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.Linq;

namespace Plagiair.Services
{

    public class ReservationsService : IReservationService
    {
        public IEnumerable<Reservation> Reservations;
        public IEnumerable<Ticket> Tickets = new List<Ticket>();
        private string Json;
        private string JsonTickets;

        public ReservationsService()
        {
            string basePath = Directory.GetCurrentDirectory();
            Json = File.OpenText($"{basePath}/Data/Reservations.json").ReadToEnd();
            JsonTickets = File.OpenText($"{basePath}/Data/Tickets.json").ReadToEnd();
            Tickets = JsonConvert.DeserializeObject<IEnumerable<Ticket>>(JsonTickets);
            Reservations = JsonConvert.DeserializeObject<IEnumerable<Reservation>>(Json);
         
        }
        public void setReservation(PreOrder preOrder)
        {
            Tickets = preOrder.Tickets;
            var fullTick = JsonConvert.SerializeObject(Tickets);
            JArray.Parse(JsonTickets).Add(fullTick);

            foreach (var ticket in preOrder.Tickets)
            {
                var reservations = new Reservation()
                {
                    Id = ticket.ReservationId,
                    Name = preOrder.Name,
                    Date = ticket.Date,
                    Ticket = ticket,
                };
                Reservations = Reservations.Append(reservations);

            }


            var fullRes = JsonConvert.SerializeObject(Reservations);
            JArray.Parse(Json).Add(fullRes);
        }

        public IEnumerable<Reservation> getAllReservations(){
            return Reservations;
        }

        public PreOrder preOrder(ReservationContract reservationContract)
        {
            PreOrder preOrder = new PreOrder()
            {
                Name = reservationContract.Name,
                    TotalPrice = calculTotalePrice(reservationContract),
                    Tickets = createTickets(reservationContract)
            };
            
            return preOrder;
        }

        public double calculTotalePrice(ReservationContract reservationConctract)
        {
            double price = 0;


            foreach(var item in reservationConctract.OptionAller)
            {
                price += item.Price;
            };


            foreach (var item in reservationConctract.OptionRetour)
            {
                price += item.Price;
            };

            foreach (var item in reservationConctract.Flights)
            {

                price += item.Price;
               
                
                if (reservationConctract.Flights.Count() == 2)
                {
                    price -= price * 0.05;
                } 

                if(item.StopOvers.Count() > 0)
                {
                    price -= price * 0.40;
                }

            }
            return price;
        }

        public IEnumerable<Ticket> createTickets(ReservationContract reservationContract)
        {
            IEnumerable<Ticket> tickets =new List<Ticket>();
            int count = 0;
            foreach (var flight in reservationContract.Flights)
            {

                count++;
                var ticket = new Ticket()
                {
                    ReservationId = reservationContract.Id,
                    Flight = flight,
          

                };
                if (count == 1)
                {
                    ticket.Option = reservationContract.OptionAller;
                    ticket.Date = reservationContract.DateIn;

                }else if (count == 2)
                {
                    ticket.Option = reservationContract.OptionRetour;
                    ticket.Date = reservationContract.DateOut;
                }

                tickets = tickets.Append(ticket);

            }

            return tickets;
        }

    }
}

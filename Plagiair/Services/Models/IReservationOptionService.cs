﻿using Plagiair.Models;
using System.Collections.Generic;

namespace Plagiair.Services.Models
{
    public interface IReservationOptionService
    {
        public IEnumerable<ReservationOption> getAllReservationOptions();
    }

}

﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Plagiair.Models;

namespace Plagiair.Services.Models
{
    public interface IOptionService
    {
        public IEnumerable<Option> getAllOption();
     
    }
}

﻿using Plagiair.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Plagiair.Services.Models
{
    public interface IExternalFlightService
    {

        public Task<IEnumerable<Flight>> GetAllExternalFlights();
    }
}

﻿using Plagiair.Models;
using System.Collections.Generic;

namespace Plagiair.Services.Models
{
    public interface IFlightOptionService
    {
        public IEnumerable<FlightOption> getAllFlightOptions();
    }

    
}

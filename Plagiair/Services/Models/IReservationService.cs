﻿using Plagiair.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Plagiair.Services.Models
{
    public interface IReservationService
    {
        public void setReservation(PreOrder preOrder); 
        public IEnumerable<Reservation> getAllReservations();
        public PreOrder preOrder(ReservationContract reservationConctract);
    }
}

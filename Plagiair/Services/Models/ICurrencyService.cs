﻿using Plagiair.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Plagiair.Services.Models
{
    public interface ICurrencyService
    {
        public Task<IEnumerable<Currency>> getAllCurrencies();
    }
}

﻿using Newtonsoft.Json;
using Plagiair.Models;
using Plagiair.Services.Models;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Plagiair.Services
{
    public class ExternalFlightService : IExternalFlightService
    {
        public IEnumerable<ExternalFlightContract> ExternalFlightContract;

        readonly IHttpClientFactory _httpClientFactory;

        public ExternalFlightService( IHttpClientFactory httpClientFactory )
        {
            _httpClientFactory = httpClientFactory;
        }

        public async Task<IEnumerable<Flight>> GetAllExternalFlights()
        {
            using ( var httpClient = _httpClientFactory.CreateClient() )
            {
                var response = await httpClient.GetAsync("https://api-6yfe7nq4sq-uc.a.run.app/flights");

                var json = await response.Content.ReadAsStringAsync();

                ExternalFlightContract = JsonConvert.DeserializeObject<IEnumerable<ExternalFlightContract>>( json );

                IList<Flight> flights = new List<Flight>();

                string basePath = Directory.GetCurrentDirectory();
                var file = File.OpenText($"{basePath}/Data/ExternalFlights.json");

                json = file.ReadToEnd();

                file.Close();

                var externalFlights = JsonConvert.DeserializeObject<IEnumerable<Flight>>(json);

                foreach ( var externalFlight in ExternalFlightContract )
                {
                    var flight = new Flight()
                    {
                        Id = 0,
                        Departure = externalFlight.Departure,
                        Arrival = externalFlight.Arrival,
                        Price = externalFlight.BasePrice,
                        MaxPeople = externalFlight.Plane.TotalSeats
                    };

                    flights.Add(flight);
                }

                externalFlights = externalFlights.Concat(flights);

                json = JsonConvert.SerializeObject( externalFlights );

                File.WriteAllText($"{basePath}/Data/ExternalFlights.json", json );

                return externalFlights;
            }
        }
    }
}

﻿using Newtonsoft.Json;
using Plagiair.Models;
using Plagiair.Services.Models;
using System.Collections.Generic;
using System.IO;

namespace Plagiair.Services
{
    public class FlightOptionService : IFlightOptionService
    {
        public IEnumerable<FlightOption> FlightOptions;

        public FlightOptionService()
        {
            string basePath = Directory.GetCurrentDirectory();
            var json = File.OpenText($"{basePath}/Data/FlightOptions.json").ReadToEnd();
            FlightOptions = JsonConvert.DeserializeObject<IEnumerable<FlightOption>>(json);
        }


        public IEnumerable<FlightOption> getAllFlightOptions()
        {
            return FlightOptions;
        }
    }
}

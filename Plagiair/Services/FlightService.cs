﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plagiair.Models;
using Plagiair.Services.Models;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Plagiair.Services
{
    public class FlightService : IFlightService
    {
        public IEnumerable<Flight> Flights;
        private string Json;
        public FlightService()
        {
            string basePath = Directory.GetCurrentDirectory();
            Json = File.OpenText($"{basePath}/Data/Flights.json").ReadToEnd();
            Flights = JsonConvert.DeserializeObject<IEnumerable<Flight>>( Json );
            FirstClassPlace();
        }

        private void FirstClassPlace()
        {

            foreach (var flight in this.Flights) {

                flight.FirstClass = (flight.MaxPeople * 10) / 100;
                this.Flights = this.Flights.Append(flight);
            };
            var fullRes = JsonConvert.SerializeObject(Flights);
            JArray.Parse(Json).Add(fullRes);

        }

        public IEnumerable<Flight> getAllFlights()
        {
            return Flights;
        }
    }
}

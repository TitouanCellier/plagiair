﻿using Newtonsoft.Json;
using Plagiair.Models;
using Plagiair.Services.Models;
using System.Collections.Generic;
using System.IO;

namespace Plagiair.Services
{
    public class ReservationOptionService : IReservationOptionService
    {
        public IEnumerable<ReservationOption> ReservationOptions;

        public ReservationOptionService()
        {
            string basePath = Directory.GetCurrentDirectory();
            var json = File.OpenText($"{basePath}/Data/ReservationOptions.json").ReadToEnd();
            ReservationOptions = JsonConvert.DeserializeObject<IEnumerable<ReservationOption>>(json);
        }


        public IEnumerable<ReservationOption> getAllReservationOptions()
        {
            return ReservationOptions;
        }

        public void setReservationOption(ReservationContract reservationContract)
        {
            foreach (var item in reservationContract.Flights)
            {

            }
        }
    }
}

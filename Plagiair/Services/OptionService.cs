﻿using Microsoft.AspNetCore.Mvc;
using Plagiair.Models;
using Plagiair.Services.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;

namespace Plagiair.Services
{
    public class OptionService : IOptionService
    {
        
        public IEnumerable<Option> Options;
        public OptionService() {

            var basePath = Directory.GetCurrentDirectory();
            var Json = File.OpenText($"{basePath}/Data/Option.json").ReadToEnd();
            Options= JsonConvert.DeserializeObject<IEnumerable<Option>>(Json);
        }

        public IEnumerable<Option> getAllOption()
        {
          return Options;
        }
    }
}

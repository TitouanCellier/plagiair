import { createRouter, createWebHashHistory } from 'vue-router';

const routes = [
  {
    path: '/',
    name: 'reservation',
    component: () => import('../views/ReservationView.vue')
  },
  {
    path: '/order',
    name: 'order',
    props: true,
    component: () => import('../views/OrderView.vue')
  }
];

const router = createRouter({
  history: createWebHashHistory(),
  routes
});

export default router;

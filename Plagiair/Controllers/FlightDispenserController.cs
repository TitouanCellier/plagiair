﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;

namespace Plagiair.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FlightDispenserController : ControllerBase
    {
        private IServiceProvider _serviceProvider;

        public FlightDispenserController(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        [HttpGet("getAllFlights")]
        public IActionResult getAllFlights()
        {
            var controller = _serviceProvider.GetService<FlightController>();
            return Ok( controller.getAllFlights() );
        }

        public IActionResult setReservation()
        {
            var controller = _serviceProvider.GetService<FlightController>();
            //Make request to set reservation
            return Ok();
        }
    }
}

﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Plagiair.Models;
using Plagiair.Services.Models;
using System.Collections.Generic;

namespace Plagiair.Controllers
{
    [ApiController]
    [Route("/api/[controller]")]
    public class ReservationsController : ControllerBase
    {
        readonly IReservationService _reservationService;

        public ReservationsController(IReservationService reservationService)
        {
            _reservationService = reservationService;
        }
        [HttpGet("getAllReservations")]
        public IActionResult getAllReservations()
        {
            return Ok(_reservationService.getAllReservations());
        }

        [HttpPut("setReservations")]
        public IActionResult setReservations(PreOrder preOrder)
        {
            _reservationService.setReservation(preOrder);
            return Ok();
        }

        [AllowAnonymous]
        [HttpPost("getPreOrder")]
        public IActionResult getPreOrder([FromBody] ReservationContract reservationContract)
        {
            return Ok(_reservationService.preOrder(reservationContract));
        }
    }
}

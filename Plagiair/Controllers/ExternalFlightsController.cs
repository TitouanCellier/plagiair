﻿using Microsoft.AspNetCore.Mvc;
using Plagiair.Models;
using Plagiair.Services.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Plagiair.Controllers
{
    [ApiController]
    [Route("/api/[controller]")]
    public class ExternalFlightsController : ControllerBase
    {
        private IExternalFlightService _externalFlightService;

        public ExternalFlightsController(IExternalFlightService externalFlightService)
        {
            _externalFlightService = externalFlightService;
        }

        [HttpGet("getAllExternalFlights")]
        public async Task<IActionResult> getAllExternalFlights()
        {
            var flights = await _externalFlightService.GetAllExternalFlights();
            return Ok( flights );
        }
    }
}

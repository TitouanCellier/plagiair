﻿using Microsoft.AspNetCore.Mvc;
using Plagiair.Services.Models;
using System.Threading.Tasks;

namespace Plagiair.Controllers
{
    [ApiController]
    [Route("/api/[controller]")]
    public class CurrencyController : ControllerBase
    {
        public ICurrencyService _currencyService;

        public CurrencyController(ICurrencyService currencyService)
        {
            _currencyService = currencyService;
        }

        [HttpGet("getAllCurrencies")]
        public async Task<IActionResult> GetAllCurrencies()
        {
            return Ok( await _currencyService.getAllCurrencies());
        }
    }
}

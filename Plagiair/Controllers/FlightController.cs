﻿using Microsoft.AspNetCore.Mvc;
using Plagiair.Models;
using Plagiair.Services.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Plagiair.Controllers
{
    [ApiController]
    [Route("/api/[controller]")]
    public class FlightController : ControllerBase
    {

        readonly IFlightService _flightService;
        readonly IFlightOptionService _flightOptionService;
        readonly IOptionService _optionService;
        public FlightController(IFlightService flightService, IOptionService optionService, IFlightOptionService flightOptionService)
        {
            _flightService = flightService;
            _optionService = optionService;
            _flightOptionService = flightOptionService;
        }

        [HttpGet("getAllFlights")]
        public IEnumerable<Flight> getAllFlights()
        {
            var options = _optionService.getAllOption();
            var flightOptions = _flightOptionService.getAllFlightOptions();
            var flights = _flightService.getAllFlights();
            foreach(var flight in flights)
            {
                foreach(var flightOption in flightOptions)
                {
                    if(flightOption.FlightId == flight.Id)
                    {
                        var availableOption = options.Single(o => o.Id == flightOption.OptionId);
                        flight.AvailableOptions = flight.AvailableOptions.Append(availableOption);
                    }
                }
            }
            return _flightService.getAllFlights();
        }
    }
}

﻿using Microsoft.AspNetCore.Mvc;
using Plagiair.Services;
using Plagiair.Services.Models;

namespace Plagiair.Controllers
{
    [ApiController]
    [Route("/api/[controller]")]
    public class OptionController : ControllerBase
    {
        readonly IOptionService _optionService;

        public OptionController(IOptionService optionService)
        {
            _optionService = optionService;
        }   

        [HttpGet("getAllOption")]
        public IActionResult getAllOption()
        {
            return Ok(_optionService.getAllOption());
        }
    }
}
